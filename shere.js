
    var controls;
    var scene1 = new THREE.Scene();
    var camera1 = new THREE.PerspectiveCamera(75, window.innerWidth/window.innerHeight, 0.1, 1000);
    var innerColor = 0xff0000,
        outerColor = 0xff9900;
    var innerSize = 55,
        outerSize = 60;    
    var renderer1= new THREE.WebGLRenderer({ antialias: true });
    renderer1.setClearColor( 0x000000, 0 ); // background

    renderer1.setSize($('.circles-inner').width(), $('.circles-inner').height());
    document.querySelector('.circles-inner').appendChild(renderer1.domElement);

    controls = new THREE.TrackballControls( camera1 );
    controls.noPan = true;
    controls.minDistance = 120;
    controls.maxDistance = 650;

    // Mesh
    var group = new THREE.Group();
    scene1.add(group);

    // Lights
    var light = new THREE.AmbientLight( 0x404040 ); // soft white light
    scene1.add( light );

    var directionalLight1 = new THREE.DirectionalLight( 0xffffff, 1 );
    directionalLight1.position.set( 0, 128, 128 );
    scene1.add( directionalLight1 );

    // Sphere Wireframe Inner
    var sphereWireframeInner = new THREE.Mesh(
      new THREE.IcosahedronGeometry( innerSize, 2 ),
      new THREE.MeshLambertMaterial({ 
        color: innerColor,
        ambient: innerColor,
        wireframe: true,
        transparent: true, 
        //alphaMap: THREE.ImageUtils.loadTexture( 'javascripts/alphamap.jpg' ),
        shininess: 0
      })
    );
    scene1.add(sphereWireframeInner);

    // Sphere Wireframe Outer
    var sphereWireframeOuter = new THREE.Mesh(
      new THREE.IcosahedronGeometry( outerSize, 3 ),
      new THREE.MeshLambertMaterial({ 
        color: outerColor,
        ambient: outerColor,
        wireframe: true,
        transparent: true,
        //alphaMap: THREE.ImageUtils.loadTexture( 'javascripts/alphamap.jpg' ),
        shininess: 0 
      })
    );
    scene1.add(sphereWireframeOuter);


    // Sphere Glass Inner
    var sphereGlassInner = new THREE.Mesh(
      new THREE.SphereGeometry( innerSize, 32, 32 ),
      new THREE.MeshPhongMaterial({ 
        color: innerColor,
        ambient: innerColor,
        transparent: true,
        shininess: 25,
        //alphaMap: THREE.ImageUtils.loadTexture( 'javascripts/twirlalphamap.jpg' ),
        opacity: 0.3,
      })
    );
    scene1.add(sphereGlassInner);

    // Sphere Glass Outer
    var sphereGlassOuter = new THREE.Mesh(
      new THREE.SphereGeometry( outerSize, 32, 32 ),
      new THREE.MeshPhongMaterial({ 
        color: outerColor,
        ambient: outerColor,
        transparent: true,
        shininess: 25,
        //alphaMap: THREE.ImageUtils.loadTexture( 'javascripts/twirlalphamap.jpg' ),
        opacity: 0.3,
      })
    );
    //scene1.add(sphereGlassOuter);

    // Particles Outer
    var geometry1 = new THREE.Geometry();
    for (i = 0; i < 35000; i++) {
      
      var x = -1 + Math.random() * 2;
      var y = -1 + Math.random() * 2;
      var z = -1 + Math.random() * 2;
      var d = 1 / Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2));
      x *= d;
      y *= d;
      z *= d;
      
      var vertex = new THREE.Vector3(
            x * outerSize,
            y * outerSize,
            z * outerSize
      );
      
      geometry1.vertices.push(vertex);

    }


    var particlesOuter = new THREE.PointCloud(geometry1, new THREE.PointCloudMaterial({
      size: 0.1,
      color: outerColor,
      //map: THREE.ImageUtils.loadTexture( 'javascripts/particletextureshaded.png' ),
      transparent: true,
      })
    );
    scene1.add(particlesOuter);

    // Particles Inner
    var geometry1 = new THREE.Geometry();
    for (i = 0; i < 35000; i++) {
      
      var x = -1 + Math.random() * 2;
      var y = -1 + Math.random() * 2;
      var z = -1 + Math.random() * 2;
      var d = 1 / Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2));
      x *= d;
      y *= d;
      z *= d;
      
      var vertex = new THREE.Vector3(
            x * outerSize,
            y * outerSize,
            z * outerSize
      );
      
      geometry1.vertices.push(vertex);

    }


    var particlesInner = new THREE.PointCloud(geometry1, new THREE.PointCloudMaterial({
      size: 0.1,
      color: innerColor,
      //map: THREE.ImageUtils.loadTexture( 'javascripts/particletextureshaded.png' ),
      transparent: true,
      })
    );
    scene1.add(particlesInner);

    // // Starfield
    var geometry1 = new THREE.Geometry();
    // for (i = 0; i < 5000; i++) {
    //   var vertex = new THREE.Vector3();
    //   vertex.x = Math.random()*2000-1000;
    //   vertex.y = Math.random()*2000-1000;
    //   vertex.z = Math.random()*2000-1000;
    //   geometry1.vertices.push(vertex);
    // }
    var starField = new THREE.PointCloud(geometry1, new THREE.PointCloudMaterial({
      size: 2,
      color: 0xffff99
      })
    );
    scene1.add(starField);


    camera1.position.z = -110;
    //camera1.position.x = mouseX * 0.05;
    //camera1.position.y = -mouseY * 0.05;
    //camera1.lookAt(scene1.position);

    var time = new THREE.Clock();

    var render = function () {  
      //camera1.position.x = mouseX * 0.05;
      //camera1.position.y = -mouseY * 0.05;
      //camera1.lookAt(scene1.position);

      sphereWireframeInner.rotation.x += 0.002;
      sphereWireframeInner.rotation.z += 0.002;
      
      sphereWireframeOuter.rotation.x += 0.001;
      sphereWireframeOuter.rotation.z += 0.001;
      
      sphereGlassInner.rotation.y += 0.005;
      sphereGlassInner.rotation.z += 0.005;

      sphereGlassOuter.rotation.y += 0.01;
      sphereGlassOuter.rotation.z += 0.01;

      particlesOuter.rotation.y += 0.0005;
      particlesInner.rotation.y -= 0.002;
      
      starField.rotation.y -= 0.002;

      var innerShift = Math.abs(Math.cos(( (time.getElapsedTime()+2.5) / 20)));
      var outerShift = Math.abs(Math.cos(( (time.getElapsedTime()+5) / 10)));

      starField.material.color.setHSL(Math.abs(Math.cos((time.getElapsedTime() / 10))), 1, 0.5);

      sphereWireframeOuter.material.color.setHSL(0, 1, outerShift);
      sphereGlassOuter.material.color.setHSL(0, 1, outerShift);
      particlesOuter.material.color.setHSL(0, 1, outerShift);

      sphereWireframeInner.material.color.setHSL(0.08, 1, innerShift);
      particlesInner.material.color.setHSL(0.08, 1, innerShift);
      sphereGlassInner.material.color.setHSL(0.08, 1, innerShift);

      sphereWireframeInner.material.opacity = Math.abs(Math.cos((time.getElapsedTime()+0.5)/0.9)*0.5);
      sphereWireframeOuter.material.opacity = Math.abs(Math.cos(time.getElapsedTime()/0.9)*0.5);


      directionalLight1.position.x = Math.cos(time.getElapsedTime()/0.5)*128;
      directionalLight1.position.y = Math.cos(time.getElapsedTime()/0.5)*128;
      directionalLight1.position.z = Math.sin(time.getElapsedTime()/0.5)*128;

      controls.update();

      renderer1.render(scene1, camera1);
      requestAnimationFrame(render); 

      $('canvas').css('z-index',2) 
    };

    render();
